FROM docker.io/nginx:alpine

RUN chown -R 1001:0 /var/cache/nginx && \
    chmod -R a+rwx /var/cache/nginx && \
    chmod -R ug+rwx /var/cache/nginx

CMD ["nginx", "-g", "daemon off;"]
